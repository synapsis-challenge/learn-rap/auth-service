package controller

import (
	"fmt"
	"strconv"
	"time"

	"learn-raa/auth-service/config"
	"learn-raa/auth-service/exception"
	"learn-raa/auth-service/helper"
	"learn-raa/auth-service/model/web"
	"learn-raa/auth-service/service"

	"github.com/gofiber/fiber/v2"
)

type AuthControllerImpl struct {
	AuthService service.AuthService
}

type AuthController interface {
	NewAuthRouter(app *fiber.App)
}

func NewAuthController(authService service.AuthService) AuthController {
	return &AuthControllerImpl{
		AuthService: authService,
	}
}

func (controller *AuthControllerImpl) NewAuthRouter(app *fiber.App) {
	app.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	app.Post("/register", controller.Register)
	app.Post("/login", controller.Login)
	app.Post("/logout", controller.Logout)
}

func (controller *AuthControllerImpl) Register(ctx *fiber.Ctx) error {
	var request web.RegisterRequest
	err := ctx.BodyParser(&request)
	if err != nil {
		exception.ErrorHandler(ctx, err)
	}

	AuthResponse, err := controller.AuthService.Register(ctx, request)

	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	action := fmt.Sprintf("register user %s", AuthResponse.NIK)
	data := web.LogCreateRequest{
		Actor:     "",
		Action:    action,
		Timestamp: time.Now(),
	}
	helper.ProduceToKafka(data, "POST.LOG", config.KafkaLogTopic)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    AuthResponse,
	})
}

func (controller *AuthControllerImpl) Login(ctx *fiber.Ctx) error {
	var request web.LoginRequest
	_ = ctx.BodyParser(&request)

	cookie, resp, err := controller.AuthService.Login(ctx, request)

	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	refreshJwt, err := helper.GenerateRefreshJwt(request.Nik)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	session, _ := strconv.Atoi(config.SessionRefreshToken)

	refreshToken := fiber.Cookie{
		Name:     "refresh_token",
		Value:    refreshJwt,
		Expires:  time.Now().Add(time.Hour * time.Duration(session)),
		HTTPOnly: true,
	}

	ctx.Cookie(&cookie)
	ctx.Cookie(&refreshToken)

	user, _ := controller.AuthService.FindUserWithNameNotDeleteByQueryTx(ctx, "nik", request.Nik)
	action := fmt.Sprintf("login user %s", user.Name)
	data := web.LogCreateRequest{
		Actor:     request.Nik,
		ActorName: user.Name,
		Category:  config.CategoryService,
		Project:   user.ProjectName,
		Action:    action,
		Timestamp: time.Now(),
	}
	helper.ProduceToKafka(data, "POST.LOG", config.KafkaLogTopic)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    resp,
	})
}

func (controller *AuthControllerImpl) Logout(ctx *fiber.Ctx) error {
	cookie := controller.AuthService.Logout()

	refreshToken := fiber.Cookie{
		Name:     "refresh_token",
		Value:    "",
		Expires:  time.Now().Add(-time.Hour),
		HTTPOnly: true,
	}

	ctx.Cookie(&cookie)
	ctx.Cookie(&refreshToken)

	//produce to kafka system log
	cookieData := ctx.Cookies("token")
	actor, _, _, _ := helper.ParseJwt(cookieData)
	user, _ := controller.AuthService.FindUserWithNameNotDeleteByQueryTx(ctx, "nik", actor)
	action := fmt.Sprintf("logout user %s", user.Name)
	data := web.LogCreateRequest{
		Actor:     actor,
		ActorName: user.Name,
		Category:  config.CategoryService,
		Project:   user.ProjectName,
		Action:    action,
		Timestamp: time.Now(),
	}
	helper.ProduceToKafka(data, "POST.LOG", config.KafkaLogTopic)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
	})
}
