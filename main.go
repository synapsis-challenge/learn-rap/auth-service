package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"learn-raa/auth-service/config"
	"learn-raa/auth-service/controller"
	"learn-raa/auth-service/repository"
	"learn-raa/auth-service/service"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/go-playground/validator"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
)

func controllers() {
	time.Local = time.UTC
	db := config.NewPostgresPool()
	validate := validator.New()

	authStore := repository.NewAuthStore(db)
	authRepository := repository.NewAuthRepository(authStore)
	authService := service.NewAuthService(authRepository, validate)
	authController := controller.NewAuthController(authService)

	app := fiber.New(fiber.Config{BodyLimit: 10 * 1024 * 1024})
	app.Use(recover.New())
	app.Use(cors.New(cors.Config{
		AllowOrigins:     "*",
		AllowMethods:     "*",
		AllowHeaders:     "*",
		AllowCredentials: true,
	}))

	authController.NewAuthRouter(app)

	host := fmt.Sprintf("%s:%s", config.ServerHost, config.ServerPort)
	err := app.Listen(host)

	log.Println(err)
}

func main() {
	time.Local = time.UTC
	broker := fmt.Sprintf("%s:%s", config.KafkaHost, config.KafkaPort)
	// topics := []string{config.KafkaTopic}
	topics := []string{config.KafkaTopicProfile, config.KafkaTopicUser}
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":     broker,
		"broker.address.family": config.KafkaAddressFamily,
		"group.id":              config.KafkaConsumerGroup,
		"session.timeout.ms":    config.KafkaSessionTimeout,
		"auto.offset.reset":     config.KafkaAutoOffsetReset,
	})

	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create consumer: %s\n", err)
		os.Exit(1)
	}

	log.Printf("Created Consumer %v\n", c)

	err = c.SubscribeTopics(topics, nil)
	if err != nil {
		log.Println()
	}
	run := true

	go controllers()

	for run {
		select {
		case sig := <-sigchan:
			log.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			ev := c.Poll(100)
			if ev == nil {
				continue
			}

			switch e := ev.(type) {
			case *kafka.Message:
				log.Printf("Message on %s:\n", e.TopicPartition)
				if e.Headers != nil {
					log.Printf("Headers: %v\n", e.Headers)
				}

				method := fmt.Sprintf("%v", e.Headers)

				if method == "[method=\"POST.USER\"]" {
					err := service.InputUser(e.Value)
					log.Println(err)
				} else if method == "[method=\"PUT.USER\"]" {
					err := service.UpdateUser(e.Value)
					log.Println(err)
				} else if method == "[method=\"DELETE.USER\"]" {
					err := service.DeleteUser(e.Value)
					log.Println(err)
				}

			case kafka.Error:
				fmt.Fprintf(os.Stderr, "Error: %v: %v\n", e.Code(), e)
				if e.Code() == kafka.ErrAllBrokersDown {
					run = false
				}
			default:
				log.Printf("Ignored %v\n", e)
			}
		}
	}

	log.Printf("Closing consumer\n")
	c.Close()
}
