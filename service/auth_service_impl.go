package service

import (
	"log"
	"strconv"
	"strings"
	"time"

	"learn-raa/auth-service/config"
	"learn-raa/auth-service/exception"
	"learn-raa/auth-service/helper"
	"learn-raa/auth-service/model/domain"
	"learn-raa/auth-service/model/web"
	"learn-raa/auth-service/repository"

	"github.com/go-playground/validator"
	"github.com/gofiber/fiber/v2"
)

type AuthServiceImpl struct {
	AuthRepository repository.AuthRepository
	Validate       *validator.Validate
}

type AuthService interface {
	Register(ctx *fiber.Ctx, request web.RegisterRequest) (web.RegisterResponse, error)
	Login(ctx *fiber.Ctx, request web.LoginRequest) (fiber.Cookie, web.LoginResponse, error)
	Logout() fiber.Cookie
	FindUserWithNameNotDeleteByQueryTx(ctx *fiber.Ctx, query, value string) (domain.UserWithName, error)
}

func NewAuthService(authRepository repository.AuthRepository, validate *validator.Validate) AuthService {
	return &AuthServiceImpl{
		AuthRepository: authRepository,
		Validate:       validate,
	}
}

func (s *AuthServiceImpl) Register(ctx *fiber.Ctx, request web.RegisterRequest) (web.RegisterResponse, error) {
	status := 1

	createdUser := domain.User{
		NIK:       request.NIK,
		Name:      request.Name,
		Email:     request.Email,
		Phone:     request.Phone,
		Status:    &status,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	createdUser.SetPassword(request.Password)

	id, err := s.AuthRepository.RegisterTx(ctx.Context(), createdUser)

	if err != nil {
		if strings.Contains(err.Error(), "unique") && strings.Contains(err.Error(), "users_nik_key") {
			return web.RegisterResponse{}, exception.ErrorBadRequest("NIK already exist.")
		}
		return web.RegisterResponse{}, err
	}

	createdUser.Id = &id

	helper.ProduceToKafka(createdUser, "POST.USER", config.KafkaTopic)
	// helper.ProduceToKafka(createdUser, "POST.USER", config.KafkaTopicProfile)
	// helper.ProduceToKafka(createdUser, "POST.USER", config.KafkaTopicUser)

	// redis
	redisKey := "user:" + createdUser.NIK

	// redis set
	err = helper.RedisSet(redisKey, createdUser)
	if err != nil {
		log.Println(err)
	}

	return domain.ToRegisterResponse(createdUser), nil
}

func (service *AuthServiceImpl) Login(ctx *fiber.Ctx, request web.LoginRequest) (fiber.Cookie, web.LoginResponse, error) {
	err := service.Validate.Struct(request)
	if err != nil {
		return fiber.Cookie{}, web.LoginResponse{}, exception.ErrorBadRequest(err.Error())
	}

	user, err := service.AuthRepository.LoginTx(ctx.Context(), request.Nik)

	if err != nil || user.NIK == "" {
		return fiber.Cookie{}, web.LoginResponse{}, exception.ErrorNotFound("User tidak ditemukan.")
	}

	if *user.Status == 0 {
		return fiber.Cookie{}, web.LoginResponse{}, exception.ErrorUnauthorize("User tidak aktif.")
	}

	if *user.Status == 2 {
		return fiber.Cookie{}, web.LoginResponse{}, exception.ErrorUnauthorize("User sudah di rehired dengan nik baru.")
	}

	err = user.ComparePassword(user.Password, request.Password)
	if err != nil {
		return fiber.Cookie{}, web.LoginResponse{}, exception.ErrorBadRequest("Password salah.")
	}

	token, err := helper.GenerateJwt(user.NIK)
	if err != nil {
		return fiber.Cookie{}, web.LoginResponse{}, exception.ErrorBadRequest(err.Error())
	}

	session, _ := strconv.Atoi(config.SessionLogin)
	cookie := fiber.Cookie{
		Name:     "token",
		Value:    token,
		Expires:  time.Now().Add(time.Hour * time.Duration(session)),
		HTTPOnly: true,
	}

	return cookie, domain.ToLoginResponse(user), nil
}

func (service *AuthServiceImpl) Logout() fiber.Cookie {
	cookie := fiber.Cookie{
		Name:     "token",
		Value:    "",
		Expires:  time.Now().Add(-time.Hour),
		HTTPOnly: true,
	}

	return cookie
}

func (service *AuthServiceImpl) FindUserWithNameNotDeleteByQueryTx(ctx *fiber.Ctx, query, value string) (domain.UserWithName, error) {

	user, err := service.AuthRepository.FindUserWithNameNotDeleteByQueryTx(ctx.Context(), query, value)
	if err != nil {
		log.Println(err)
	}

	return user, nil
}
