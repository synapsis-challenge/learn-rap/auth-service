package config

import (
	"strconv"

	"learn-raa/auth-service/utils"
)

var (
	//db
	host     = utils.GetEnv("DB_HOST")
	port     = utils.GetEnv("DB_PORT")
	user     = utils.GetEnv("DB_USERNAME")
	password = utils.GetEnv("DB_PASSWORD")
	dbName   = utils.GetEnv("DB_NAME")
	minConns = utils.GetEnv("DB_POOL_MIN")
	maxConns = utils.GetEnv("DB_POOL_MAX")

	//host
	ServerHost     = utils.GetEnv("SERVER_URI")
	ServerPort     = utils.GetEnv("SERVER_PORT")
	EndpointPrefix = utils.GetEnv("ENDPOINT_PREFIX")
	DefaultLimit   = utils.GetEnv("DEFAULT_LIMIT")

	//jwt
	SecretKey           = utils.GetEnv("SECRET_KEY")
	SecretKeyRefresh    = utils.GetEnv("SECRET_KEY_REFRESH")
	SessionLogin        = utils.GetEnv("SESSION_LOGIN")
	SessionRefreshToken = utils.GetEnv("SESSION_REFRESH_TOKEN")

	//variable
	PrefixFinger           = utils.GetEnv("PREFIX_FINGER")
	VariableRoleAdmin      = utils.GetEnv("ROLE_ADMIN")
	VariableRoleUser       = utils.GetEnv("ROLE_USER")
	CategoryService        = utils.GetEnv("CATEGORY_SERVICE")
	PermissionAddUser      = utils.GetEnv("PERMISSION_ADD_USER")
	PermissionImportUser   = utils.GetEnv("PERMISSION_IMPORT_USER")
	PermissionManageUser   = utils.GetEnv("PERMISSION_MANAGE_USER")
	PermissionActivateUser = utils.GetEnv("PERMISSION_ACTIVATE_USER")
	PermissionDeactiveUser = utils.GetEnv("PERMISSION_DEACTIVE_USER")

	//minio
	Minio_storage_url = utils.GetEnv("MINIO")
	Minio_storage_    = utils.GetEnv("MINIO")
	MinioPrefix       = utils.GetEnv("MINIO_PREFIX")
	MinioAccessKey    = utils.GetEnv("MINIO_ACCESSKEY")
	MinioSecretKey    = utils.GetEnv("MINIO_SECRETKEY")
	MinioEndpoint     = utils.GetEnv("MINIO_ENDPOINT")
	MinioBucket       = utils.GetEnv("MINIO_BUCKET")
	MinioPath         = utils.GetEnv("MINIO_PATH")
	MinioSSL          = utils.GetEnv("MINIO_SSL")

	//kafka
	KafkaHost              = utils.GetEnv("KAFKA_HOST")
	KafkaPort              = utils.GetEnv("KAFKA_PORT")
	KafkaTopic             = utils.GetEnv("KAFKA_TOPIC")
	KafkaLogTopic          = utils.GetEnv("KAFKA_LOG_TOPIC")
	KafkaTopicDepartement  = utils.GetEnv("KAFKA_TOPIC_DEPARTEMENT")
	KafkaTopicSiteLocation = utils.GetEnv("KAFKA_TOPIC_SITE_LOCATION")
	KafkaTopicProfile      = utils.GetEnv("KAFKA_TOPIC_PROFILE")
	KafkaTopicUser         = utils.GetEnv("KAFKA_TOPIC_USER")
	KafkaConsumerGroup     = utils.GetEnv("KAFKA_CONSUMER_GROUP")
	KafkaAddressFamily     = utils.GetEnv("KAFKA_ADDRESS_FAMILY")
	KafkaSessionTimeout, _ = strconv.Atoi(utils.GetEnv("KAFKA_SESSION_TIMEOUT"))
	KafkaAutoOffsetReset   = utils.GetEnv("KAFKA_AUTO_OFFSET_RESET")

	//redis
	RedisHost = utils.GetEnv("REDIS_HOST")
	RedisPort = utils.GetEnv("REDIS_PORT")
)
