package repository

import (
	"context"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Store interface {
	WithTransaction(ctx context.Context, fn func(pgx.Tx) error) error
}

type StoreImpl struct {
	Database *pgxpool.Pool
}

func NewAuthStore(database *pgxpool.Pool) Store {
	return &StoreImpl{
		Database: database,
	}
}

func (r *StoreImpl) WithTransaction(ctx context.Context, fn func(pgx.Tx) error) error {
	tx, err := r.Database.Begin(ctx)
	if err != nil {
		return err
	}

	if err := fn(tx); err != nil {
		_ = tx.Rollback(ctx)
		return err
	}

	px := tx.Commit(ctx)

	return px
}

func (r *StoreImpl) WithoutTransaction(ctx context.Context, fn func(pgxpool.Pool) error) error {
	if err := fn(*r.Database); err != nil {
		return err
	}

	return nil
}
