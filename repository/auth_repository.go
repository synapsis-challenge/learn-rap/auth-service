package repository

import (
	"context"

	"learn-raa/auth-service/model/domain"
)

type AuthRepository interface {
	RegisterTx(ctx context.Context, user domain.User) (int, error)
	LoginTx(ctx context.Context, nik string) (domain.User, error)
	FindUserNotDeleteByQueryTx(ctx context.Context, query, value string) (domain.User, error)
	UpdatePasswordTx(ctx context.Context, user domain.User) error
	FindUserWithNameNotDeleteByQueryTx(ctx context.Context, query, value string) (domain.UserWithName, error)
}
