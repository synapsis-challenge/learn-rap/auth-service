package repository

import (
	"context"

	"learn-raa/auth-service/model/domain"

	"github.com/jackc/pgx/v5"
)

type AuthRepositoryImpl struct {
	DB Store
}

func NewAuthRepository(db Store) AuthRepository {
	return &AuthRepositoryImpl{
		DB: db,
	}
}

func (repository *AuthRepositoryImpl) RegisterTx(ctx context.Context, user domain.User) (int, error) {
	var id int
	var err error
	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		id, err = repository.Register(ctx, tx, user)
		if err != nil {
			return err
		}

		return nil
	})

	return id, err
}

func (repository *AuthRepositoryImpl) LoginTx(ctx context.Context, nik string) (domain.User, error) {

	var data domain.User
	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		data, err = repository.Login(ctx, tx, nik)
		if err != nil {
			return err
		}

		return nil
	})

	return data, err
}

func (repository *AuthRepositoryImpl) FindUserNotDeleteByQueryTx(ctx context.Context, query, value string) (domain.User, error) {

	var data domain.User
	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		data, err = repository.FindUserNotDeleteByQuery(ctx, tx, query, value)
		if err != nil {
			return err
		}

		return nil
	})

	return data, err
}

func (repository *AuthRepositoryImpl) UpdatePasswordTx(ctx context.Context, user domain.User) error {

	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		err = repository.UpdatePassword(ctx, tx, user)
		if err != nil {
			return err
		}

		return nil
	})

	return err
}

func (repository *AuthRepositoryImpl) FindUserWithNameNotDeleteByQueryTx(ctx context.Context, query, value string) (domain.UserWithName, error) {

	var data domain.UserWithName
	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		data, err = repository.FindUserWithNameNotDeleteByQuery(ctx, tx, query, value)
		if err != nil {
			return err
		}

		return nil
	})

	return data, err
}
