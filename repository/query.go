package repository

import (
	"context"
	"fmt"

	"learn-raa/auth-service/model/domain"

	"github.com/jackc/pgx/v5"
)

func (repository *AuthRepositoryImpl) Register(ctx context.Context, db pgx.Tx, user domain.User) (int, error) {
	query := fmt.Sprintf("INSERT INTO %s (nik, name, email, password, phone, status, created_at, updated_at, nomor_wa) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9) RETURNING id", "users")
	_, err := db.Prepare(context.Background(), "register", query)
	if err != nil {
		return 0, err
	}

	ret, err := db.Query(context.Background(), "register",
		user.NIK, user.Name, user.Email, user.Password, user.Phone, user.Status, user.CreatedAt, user.UpdatedAt, user.NomorWa)

	if err != nil {
		return 0, err
	}

	defer ret.Close()
	var id int
	ret.Next()
	ret.Scan(&id)
	if ret.Err() != nil {
		return 0, ret.Err()
	}

	return id, nil
}

func (repository *AuthRepositoryImpl) Login(ctx context.Context, db pgx.Tx, nik string) (domain.User, error) {
	queryStr := fmt.Sprintf("SELECT * FROM %s WHERE LOWER(nik) = LOWER($1) AND deleted_at is NULL", "users")

	user, err := db.Query(context.Background(), queryStr, nik)

	if err != nil {
		return domain.User{}, err
	}

	defer user.Close()

	data, err := pgx.CollectOneRow(user, pgx.RowToStructByPos[domain.User])

	if err != nil {
		return domain.User{}, err
	}

	return data, nil
}

func (repository *AuthRepositoryImpl) FindUserNotDeleteByQuery(ctx context.Context, db pgx.Tx, query, value string) (domain.User, error) {
	queryStr := fmt.Sprintf("SELECT * FROM %s WHERE %s = $1 AND deleted_at is NULL", "users", query)

	user, err := db.Query(context.Background(), queryStr, value)

	if err != nil {
		return domain.User{}, err
	}

	defer user.Close()

	data, err := pgx.CollectOneRow(user, pgx.RowToStructByPos[domain.User])

	if err != nil {
		return domain.User{}, err
	}

	return data, nil
}

func (repository *AuthRepositoryImpl) UpdatePassword(ctx context.Context, db pgx.Tx, user domain.User) error {
	query := "UPDATE users SET password = $1 WHERE nik = $2"
	_, err := db.Prepare(context.Background(), "update_pass", query)
	if err != nil {
		return err
	}

	_, err = db.Exec(context.Background(), "update_pass", user.Password, user.NIK)
	if err != nil {
		return err
	}

	return nil
}

func (repository *AuthRepositoryImpl) FindUserWithNameNotDeleteByQuery(ctx context.Context, db pgx.Tx, query, value string) (domain.UserWithName, error) {
	queryStr := fmt.Sprintf(`SELECT u.* FROM %s u
	
	WHERE %s = $1 AND deleted_at is NULL`, "users", query)

	user, err := db.Query(context.Background(), queryStr, value)

	if err != nil {
		return domain.UserWithName{}, err
	}

	defer user.Close()

	data, err := pgx.CollectOneRow(user, pgx.RowToStructByPos[domain.UserWithName])

	if err != nil {
		return domain.UserWithName{}, err
	}

	return data, nil
}
