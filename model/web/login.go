package web

type LoginRequest struct {
	Nik      string `json:"nik" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type ForgetPassowrd struct {
	Email string `json:"email" validate:"required"`
}

type ResetPassword struct {
	Password        string `json:"password" validate:"required"`
	PasswordConfirm string `json:"password_confirm" validate:"required"`
}

// Response
type LoginResponse struct {
	NIK    string `json:"nik"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Phone  string `json:"phone"`
	Status *int   `json:"status"`
}

type TokenResponse struct {
	Token string `json:"token"`
}
