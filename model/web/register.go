package web

type RegisterRequest struct {
	NIK      string `json:"nik"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Phone    string `json:"phone"`
}

// Response
type RegisterResponse struct {
	NIK    string `json:"nik"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Phone  string `json:"phone"`
	Status *int   `json:"status"`
}
