package domain

import (
	"time"

	"learn-raa/auth-service/model/web"

	"golang.org/x/crypto/bcrypt"
)

// will be removed - using this struct for temporary cause still used in many action query
type User struct {
	Id        *int       `json:"id"`
	NIK       string     `json:"nik"`
	Name      string     `json:"name"`
	Email     string     `json:"email"`
	Password  string     `json:"password"`
	Phone     string     `json:"phone"`
	Status    *int       `json:"status"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`
	NomorWa   string     `json:"nomor_wa"`
}

// this is the relevan field data type to database
type UserDomain struct {
	Id        *int       `json:"id"`
	NIK       string     `json:"nik"`
	Name      string     `json:"name"`
	Email     string     `json:"email"`
	Password  string     `json:"password"`
	Phone     *string    `json:"phone"`
	Status    *int       `json:"status"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`
	NomorWa   *string    `json:"nomor_wa"`
}

type RehiredUser struct {
	LastNik string `json:"last_nik"`
	Nik     string `json:"nik"`
}

type UserImported struct {
	Id      int     `json:"id"`
	NIK     string  `json:"nik"`
	Email   string  `json:"email"`
	Phone   *string `json:"phone"`
	NomorWa *string `json:"nomor_wa"`
}

func UserDomainTouser(user UserDomain) User { //temporary

	nilString := ""
	nilTime := time.Time{}

	if user.Phone == nil {
		user.Phone = &nilString
	}

	if user.NomorWa == nil {
		user.NomorWa = &nilString
	}

	if user.CreatedAt == nil {
		user.CreatedAt = &nilTime
	}

	if user.UpdatedAt == nil {
		user.UpdatedAt = &nilTime
	}

	return User{
		Id:        user.Id,
		NIK:       user.NIK,
		Name:      user.Name,
		Email:     user.Email,
		Password:  user.Password,
		Phone:     *user.Phone,
		Status:    user.Status,
		CreatedAt: *user.CreatedAt,
		UpdatedAt: *user.UpdatedAt,
		DeletedAt: user.DeletedAt,
		NomorWa:   *user.NomorWa,
	}

}

func (user *User) SetPassword(password string) {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(password), 14)
	user.Password = string(hashedPassword)
}

func (user *User) ComparePassword(correctPassword string, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(correctPassword), []byte(password))
}

func ToRegisterResponse(user User) web.RegisterResponse {
	return web.RegisterResponse{
		NIK:    user.NIK,
		Name:   user.Name,
		Email:  user.Email,
		Phone:  user.Phone,
		Status: user.Status,
	}
}

func ToLoginResponse(user User) web.LoginResponse {
	return web.LoginResponse{
		NIK:    user.NIK,
		Name:   user.Name,
		Email:  user.Email,
		Phone:  user.Phone,
		Status: user.Status,
	}
}

type UserWithName struct {
	Id              *int       `json:"id"`
	NIK             string     `json:"nik"`
	RoleId          *int       `json:"role_id"`
	DepartementId   *int       `json:"departement_id"`
	ProjectId       *int       `json:"project_id"`
	Name            string     `json:"name"`
	Email           string     `json:"email"`
	Password        string     `json:"password"`
	Phone           string     `json:"phone"`
	Status          *int       `json:"status"`
	ImageProfile    string     `json:"image_profile"`
	FingerId        string     `json:"finger_id"`
	CreatedAt       time.Time  `json:"created_at"`
	UpdatedAt       time.Time  `json:"updated_at"`
	DeletedAt       *time.Time `json:"deleted_at"`
	FaceId          string     `json:"face_id"`
	PositionId      *int       `json:"position_id"`
	NomorWa         string     `json:"nomor_wa"`
	InactiveReason  string     `json:"inactive_reason"`
	Gender          *int       `json:"gender"`
	Religion        *int       `json:"religion"`
	DateOfHire      time.Time  `json:"date_of_hire"`
	RoleName        string     `json:"role_name"`
	DepartementName string     `json:"departement_name"`
	ProjectName     string     `json:"project_name"`
	PositionName    string     `json:"position_name"`
}
